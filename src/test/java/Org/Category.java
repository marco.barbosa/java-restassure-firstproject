package Org;

public class Category {
    private int id;
    private String name;
    public int getID() { return id; }
    public void setID(int value) { this.id = value; }
    public String getName() { return name; }
    public void setName(String value) { this.name = value; }
}
