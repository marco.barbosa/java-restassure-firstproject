package TreinamentoRestAssured;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

public class OrderTests {
    @Test
    public void CreatePetOrder(){
        List<Header> headerlist = new ArrayList<>();
        headerlist.add(new Header("content-type", "application/json"));
        headerlist.add(new Header("accept", "application/json"));
        Headers headers = new Headers(headerlist);

        JSONObject petOrder = new JSONObject();
        petOrder.put("id", 30335410);
        petOrder.put("petId", 2345678);
        petOrder.put("quantity", 1);
        petOrder.put("shipDate", "2021-03-10T17:59:13.879Z");
        petOrder.put("status", "placed");
        petOrder.put("complete", true);
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/store/order").
                header("content-type", "application/json").
                body(petOrder).
        when().
                post().
        then().
                body("id", equalTo(30335410),
                "petId", equalTo(2345678),
                "quantity", equalTo(1)
        );
    }
}

