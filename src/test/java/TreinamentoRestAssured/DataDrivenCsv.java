package TreinamentoRestAssured;

import Org.ReadCsvFile;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DataDrivenCsv {
    @DataProvider(name = "dataPetCSVProvider")
    public Iterator<Object []> dataPetProvider2(){
        ReadCsvFile csv = new ReadCsvFile();
        return csv.csvProvider("src/test/java/org/petData.csv");
    }
    @Test(dataProvider = "dataPetCSVProvider")
    public void testeComDataDriven2(String[] petData) {
        int petId = Integer.parseInt(petData[0]);
        int categoryId = Integer.parseInt(petData[1]);
        String categoryName = petData[2];
        String petName = petData[3];
        String petPhotoURL1 = petData[4];
        int tag1Id = Integer.parseInt(petData[5]);
        String tag1Name = petData[6];
        String petStatus = petData[7];

        JSONObject pet = new JSONObject();
        JSONObject category = new JSONObject();
        JSONObject tag1 = new JSONObject();
        JSONArray tags = new JSONArray();
        JSONArray photoURLs = new JSONArray();

        pet.put("id", petId);
        category.put("id", categoryId);
        category.put("name", categoryName);
        pet.put("category", category);
        pet.put("name", petName);
        photoURLs.add(petPhotoURL1);
        pet.put("photoUrls", photoURLs);
        tag1.put("id", tag1Id);
        tag1.put("name", tag1Name);
        tags.add(tag1);
        pet.put("tags", tags);
        pet.put("status", petStatus);
        given()
                .baseUri("https://petstore.swagger.io/v2")
                .header("content-type", "application/json")
                .basePath("/pet")
                .body(pet)
        .when()
                .post()
        .then()
                .statusCode(200)
                .body("id", equalTo(petId),
                        "category.id", equalTo(categoryId),
                        "category.name", equalTo(categoryName),
                        "name", equalTo(petName),
                        "photoUrls[0]", equalTo(petPhotoURL1),
                        "tags[0].id", equalTo(tag1Id),
                        "tags[0].name", equalTo(tag1Name),
                        "status", equalTo(petStatus));

    }
}
