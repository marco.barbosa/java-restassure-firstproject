package TreinamentoRestAssured;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class WrongMethodTests {
    @Test
    public void GetMethod() {
        // recebe o pet
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet").
        when().
                get().
        then().
                statusCode(405);
    }
}
