package TreinamentoRestAssured;

import java.lang.Object;

import Org.Category;
import Org.Pet;
import Org.Tag;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class DataDrivenTest {
    @DataProvider(name = "dataPetObjectProvider")
    public Object[] dataPetProvider() {
        //PET1
        Pet pet1 = new Pet();
        pet1.setID(1111);
        Category category = new Category();
        category.setID(1111);
        category.setName("felino");
        pet1.setCategory(category);
        pet1.setName("Shepherd");
        pet1.setPhotoUrls(new String[]{"http://fotosdegato.com.br/shepherd1.png"});
        Tag tag1 = new Tag();
        tag1.setID(1111);
        tag1.setName("Sem raça definida");
        pet1.setTags(new Tag[]{tag1});
        pet1.setStatus("available");

        //PET2
        Pet pet2 = new Pet();
        pet2.setID(2222);
        category = new Category();
        category.setID(2222);
        category.setName("felino");
        pet2.setCategory(category);
        pet2.setName("Martini");
        pet2.setPhotoUrls(new String[]{"http://fotosdegato.com.br/martini.png"});

        Tag tag2 = new Tag();
        tag2.setID(1111);
        tag2.setName("Sem raça definida");
        pet2.setTags(new Tag[]{tag1});
        pet2.setStatus("available");

        Object[] Pet = new Object[]{ pet1, pet2 };
        return Pet;

        // return new Pet[]{ pet1, pet2 };
    }
    @Test(dataProvider = "dataPetObjectProvider")
    public void testeComDataDriven(Pet petData) {
        // System.out.println(petData.getID());

        given()
                .baseUri("https://petstore.swagger.io/v2")
                .header("content-type", "application/json")
                .basePath("/pet")
                .body(petData)
                .when()
                .post()
                .then()
                .statusCode(200)
                .body("id", equalTo(petData.getID()),
                        "category.id", equalTo(petData.getCategory().getID()),
                        "category.name", equalTo(petData.getCategory().getName()),
                        "name", equalTo(petData.getName()),
                        "photoUrls[0]", equalTo(petData.getPhotoUrls()[0]),
                        "tags[0].id", equalTo(petData.getTags()[0].getID()),
                        "tags[0].name", equalTo(petData.getTags()[0].getName()),
                        "status", equalTo(petData.getStatus()));
    }
}
