package TreinamentoRestAssured;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class GetPetTests {
    @Test
    public void GetPetTest(){
        List<Header> headerlist = new ArrayList<>();
        headerlist.add(new Header("content-type", "application/json"));
        headerlist.add(new Header("accept", "application/json"));
        Headers headers = new Headers(headerlist);

        JSONObject pet = new JSONObject();
        JSONObject category = new JSONObject();
        JSONObject tag1 = new JSONObject();
        JSONObject tag2 = new JSONObject();
        JSONArray tags = new JSONArray();
        JSONArray photoURLs = new JSONArray();

        pet.put("id", 2345678);
        pet.put("name", "Epaminondas");
        pet.put("status", "available");

        category.put("id", 7);
        category.put("name", "gato");
        pet.put("category", category);

        tag1.put("id", 0);
        tag1.put("name", "Sem raça definida");
        tag2.put("id", 1);
        tag2.put("name", "Preto");
        tags.add(tag1);
        tags.add(tag2);
        pet.put("tags", tags);

        photoURLs.add("fotosdegato.com.br/foto1.png");
        photoURLs.add("fotosdegato.com.br/foto2.png");

        pet.put("photoUrls", photoURLs);

        // cria o pet
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet").
                headers(headers).
                body(pet).
        when().
                put().
        then().
                statusCode(200);

        // recebe o pet
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/{petId}").
                pathParam("petId",2345678).
        when().
                get().
        then().
                statusCode(200);
    }
    @Test
    public void GetNoExistingPetTest(){
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/{petId}").
                pathParam("petId",000000).
        when().
                get().

        then().
                statusCode(404).
                body("message", equalTo("Pet not found"),
                        "type", equalTo("error"),
                        "code", equalTo(1)
                );
    }
    @Test
    public void GetPetByStatus() {
        // busca pelo status
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/findByStatus").
                headers("content-type", "application/x-www-form-urlencoded", "accept", "application/json").
                formParam("status", "pending").
        when().
                get().
        then().
                statusCode(200).
                body(not(emptyArray()));
    }
}


