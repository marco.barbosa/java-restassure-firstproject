package TreinamentoRestAssured;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class UpdatePetTests {
    @Test
    public void UpdatePetTest(){
        List<Header> headerlist = new ArrayList<>();
        headerlist.add(new Header("content-type", "application/json"));
        headerlist.add(new Header("accept", "application/json"));
        Headers headers = new Headers(headerlist);

        JSONObject pet = new JSONObject();
        JSONObject category = new JSONObject();
        JSONObject tag1 = new JSONObject();
        JSONObject tag2 = new JSONObject();
        JSONArray tags = new JSONArray();
        JSONArray photoURLs = new JSONArray();

        pet.put("id", 2345678);
        pet.put("name", "Epaminondas");
        pet.put("status", "available");

        category.put("id", 7);
        category.put("name", "gato");
        pet.put("category", category);

        tag1.put("id", 0);
        tag1.put("name", "Sem raça definida");
        tag2.put("id", 1);
        tag2.put("name", "Preto");
        tags.add(tag1);
        tags.add(tag2);
        pet.put("tags", tags);

        photoURLs.add("fotosdegato.com.br/foto1.png");
        photoURLs.add("fotosdegato.com.br/foto2.png");

        pet.put("photoUrls", photoURLs);

        // cria o pet
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet").
                headers(headers).
                body(pet).

        when().
                put().
        then().
                statusCode(200);

        // atualiza o pet
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/{petId}").
                pathParam("petId",2345678).
                headers("content-type", "application/x-www-form-urlencoded", "accept", "application/json").
                formParam("name", "Xondas").
        when().
                post().

        then().
                statusCode(200).
                body("message", equalTo("2345678"),
                "code", equalTo(200)
                );
    }
    @Test
    public void UpdatePetWrongIdTest() {
        // atualiza o pet com id errado
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/{petId}").
                pathParam("petId",0000000).
                headers("content-type", "application/x-www-form-urlencoded", "accept", "application/json").
                formParam("name", "Xondas").
        when().
                post().

        then().
                statusCode(404).
                body("message", equalTo("not found"),
                        "code", equalTo(404)
                );
    }
}
